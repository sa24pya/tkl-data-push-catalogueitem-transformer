# TKL data-push-catalogueitem-transformer
TKL data push catalogueitem transformer  
TKL data push catalogueitem transformer provides catalogueitem tranformation for push.  
Support PSR-1, PSR-2, PSR-3, PSR-4.

![sa24 package repository](https://www.dream-destination-wedding.com/images/exotic2.jpg)

## Install

Via Composer

``` bash
$ composer require tkl/data-import-directory-processor
```

## Usage

``` bash
$ php
$ 
```

## Testing

``` bash
$ phpunit
```

## Contributing


## Credits

- [Wilson Smith](https://github.com/wilsonsmith)
- [All Contributors](ttps://mma-sa24@bitbucket.org/tkl/data-push-catalogueitem-transformer.git/contributors)

## License

The GNU General Public Licence version 3 (GPLv3). Please see [License File](LICENSE.md) for more information.
