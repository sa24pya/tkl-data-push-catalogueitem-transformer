<?php
/**
 * File: Transformer.php
 *
 * @author Jesus Coll Cerdan jesus.coll@basetis.com, bcn
 * @version 1.0
 * @package application
 */

/**
 * Class CatalogueItemTransformer
 * Transform the product data-structure, according to
 * the serviºce in which the product will be pushed.
 * @package application
 * @subpackage DataAccess
 */

namespace DataPushCatalogueItemTransformer;

/**
* Transformer Class:
*/
class CatalogueItemTransformer
{
    /**
     * @var array : The failed gtins.
     */
    public $failedGTINs = [];

    /**
     * @var array : The reference of the fields position inside a gs1/sa24 item
     */
    protected static $reference = array(
        'gtin' => array("tradeItem->gtin"),

        'supplier_gln' => array("tradeItem->informationProviderOfTradeItem->gln"),

        'gln' => array("tradeItem->informationProviderOfTradeItem->gln"),

        'cvr_number' => array(
            "tradeItem->informationProviderOfTradeItem->vat->cvrNumber->@value",
            "tradeItem->informationProviderOfTradeItem->vat->cvrNumber->@attributes->languageCode",
        ),

        'sku' => array(
            "tradeItem->gtin",
            "tradeItem->ean",
            "tradeItem->informationProviderOfTradeItem->vat->cvrNumber->@value",
            ),

        'article_number' => array(
            "tradeItem->additionalTradeItemIdentification->@value",
            "tradeItem->additionalTradeItemIdentification->@attributes->additionalTradeItemIdentificationTypeCode",
        ),

        'description' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_description:tradeItemDescriptionModule->tradeItemDescriptionInformation->".
            "descriptionShort"),

        'name' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_description:tradeItemDescriptionModule->tradeItemDescriptionInformation->".
            "functionalName->@value"),

        'short_description' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_description:tradeItemDescriptionModule->tradeItemDescriptionInformation->".
            "descriptionShort"),

        'net_weight' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule->tradeItemMeasurements->".
            "tradeItemWeight->netWeight->@value"),

        'net_weight_measurement' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule->tradeItemMeasurements->".
            "tradeItemWeight->netWeight->@attributes->measurementUnitCode"),

        'weight' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule->tradeItemMeasurements->".
            "tradeItemWeight->netWeight->@value"),

        'gross_weight' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule->tradeItemMeasurements->".
            "tradeItemWeight->grossWeight->@value"),

        'depth' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule".
            "->tradeItemMeasurements->depth->@value"),

        'depth_measurement' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule".
            "->tradeItemMeasurements->depth->@attributes->measurementUnitCode"),

        'height' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule".
            "->tradeItemMeasurements->height->@value"),

        'height_measurement' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule".
            "->tradeItemMeasurements->height->@attributes->measurementUnitCode"),

        'width' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule".
            "->tradeItemMeasurements->width->@value"),

        'width_measurement' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule".
            "->tradeItemMeasurements->width->@attributes->measurementUnitCode"),

        'manufacturer_name' => array("tradeItem->informationProviderOfTradeItem->partyName"),

        'brand_name' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_description:tradeItemDescriptionModule->tradeItemDescriptionInformation->".
            "brandNameInformation->brandName"),

        'min_stock' => array("tradeItem->tradeItemInformation->extension->".
        "trade_item_stock_information:tradeItemStockInformationModule->warehouse->rules->minStock"),

        'lead_time' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_stock_information:tradeItemStockInformationModule->warehouse->rules->leadTime"),

        'notification_days' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_stock_information:tradeItemStockInformationModule->warehouse->rules->notificationDays"),

        'manufacturer_gln' => array("tradeItem->manufacturerOfTradeItem->gln"),

        'tax_agency_code' => array("tradeItem->tradeItemInformation->extension->".
            "duty_fee_tax_information:dutyFeeTaxInformationModule".
            "->dutyFeeTaxInformation->dutyFeeTaxAgencyCode"),

        'allergen_statement' => array("tradeItem->tradeItemInformation->extension->".
            "allergen_information:AllergenInformationModule->allergenStatement"),

        'country_iso_code' => array("tradeItem->targetMarket->targetMarketCountryCode"),

        'net_content' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule".
            "->tradeItemMeasurements->netContent->@value"),

        'net_content_measurement' => array("tradeItem->tradeItemInformation->extension->".
            "trade_item_measurements:tradeItemMeasurementsModule".
            "->tradeItemMeasurements->netContent->@attributes->measurementUnitCode"),

        'food_beverage_nutrient_info' => array("tradeItem->tradeItemInformation->extension".
            "->food_and_beverage_ingredient:foodAndBeverageIngredientModule".
            "->ingredientStatement->@value"),

        'is_base_price_declaration_rel' => array("tradeItem->tradeItemInformation->extension->".
            "packaging_marking:packagingMarkingModule->packagingMarking->isPriceOnPack"), // integer

        'min_storage_temp' => array("tradeItem->tradeItemInformation->extension".
            "->trade_item_temperature_information:tradeItemTemperatureInformationModule->".
            "tradeItemTemperatureInformation->minimumTemperature->@value"),

        'min_storage_temp_measurement' => array("tradeItem->tradeItemInformation->extension".
            "->trade_item_temperature_information:tradeItemTemperatureInformationModule->".
            "tradeItemTemperatureInformation->minimumTemperature->@attributes->temperatureMeasurementUnitCode"),

        'max_storage_temp' => array("tradeItem->tradeItemInformation->extension".
            "->trade_item_temperature_information:tradeItemTemperatureInformationModule->".
            "tradeItemTemperatureInformation->maximumTemperature->@value"),

        'max_storage_temp_measurement' => array("tradeItem->tradeItemInformation->extension".
            "->trade_item_temperature_information:tradeItemTemperatureInformationModule->".
            "tradeItemTemperatureInformation->maximumTemperature->@attributes->temperatureMeasurementUnitCode"),

        'packaging_type_code' => array("tradeItem->tradeItemInformation->extension".
            "->sales_information:salesInformationModule.salesInformation".
            "->priceComparisonMeasurement->@value"),

        'image' => array("tradeItem->tradeItemInformation->extension->".
            "referenced_file_detail_information:referencedFileDetailInformationModule->".
            "referencedFileHeader->uniformResourceIdentifier"),

        'is_trade_item_a_consumer_unit' => array('tradeItem->isTradeItemAConsumerUnit'), // integer

        'is_packaging_marked_returnable' => array("tradeItem->tradeItemInformation->extension->".
            "packaging_marking:packagingMarkingModule->packagingMarking->isPackagingMarkedReturnable"), // integer

        'ean' => array( "tradeItem->ean" ), // can be empty

        'price' => array("trade_item_prices:tradeItemPricesModule->tradeItemPrices->@value->default"), // default=12
    );
    /**
    * [getCorrectValue: Obtain the correct value from a list of locations inside of a sa24/gs1 item]
    * @param  {stdclass} $SA24Item          [The SA24_catalogue item]
    * @param  {stdclass} $GS1Item           [The GS1_catalogue item]
    * @param  {array} $possibleLocations [Possible destinations]
    * @return {string}                    [The value]
    */
    protected static function getCorrectValue($SA24Item, $GS1Item, array $possibleLocations)
    {
        foreach ($possibleLocations as $possibleLocation) {
            $value = self::getValueByLocation($SA24Item, $possibleLocation);
            if (!empty($value)) {
                return $value;
            } else {
                $value = self::getValueByLocation($GS1Item, $possibleLocation);
                if (!empty($value)) {
                    return $value;
                }
            }
        }
        return '';
    }

    /**
    * gets the value of a subproperty given a propertyPath
    * @param  {stdClass} $object the object
    * @param  {propeties} array   $properties   the path of the subproperty
    * @return {mixed}         [The value from location]
    */
    protected static function getValueByLocation($object, $properties)
    {
        foreach (explode('->', $properties) as $property) {
            if (empty($object->{$property})) {
                return '';
            }
            $object = $object->{$property};
        }
        return $object;
    }

    /**
     * transform() : Entry point for this class, transforms an array of catalogueItems into magmi "csv" rows
     * @param array $catalogue : The catalogueItem list
     * @return array : The "csv" rows ready to be pushed
     */
    public function transform(array $catalogue)
    {
        $rows = [];
        // frif: TODO - replace with Class CatalogueItemModel
        // victor: frif, is this todo comment still valid?
        foreach ($catalogue['SA24'] as $SA24) {
            try {
                $row = [];
                
                // gtin number that we are looking for
                $gtinToLookFor = $SA24->tradeItem->gtin;

                // filter takes two arguments, the original array and a callback function,
                // which returns true or false based on the desired logic
                $GS1 = array_filter($catalogue['GS1'], function ($catalogueItem) use ($gtinToLookFor) {
                    return $catalogueItem ->tradeItem->gtin === $gtinToLookFor;
                });
                if (empty($GS1)) {
                    throw new \Exception($SA24->tradeItem->gtin);
                    break;
                }
                $gs1Key = key($GS1);
                foreach (self::$reference as $key => $value) {
                    $row[$key] = self::getCorrectValue($SA24, $GS1[$gs1Key], $value);
                }
                // specific manipulations
                $row['is_trade_item_a_consumer_unit'] = (int) $row['is_trade_item_a_consumer_unit'] === "true";
                $row['is_packaging_marked_returnable'] = (int) $row['is_packaging_marked_returnable'] === "true";
                $row['is_base_price_declaration_rel'] = (int) $row['is_base_price_declaration_rel'] === "true";
                $row['visibility'] = 4;
                $row['min_storage_temp'] = (!empty($row['min_storage_temp_measurement'])
                    && empty($row['min_storage_temp'])) ? "0" : $row['min_storage_temp'];
                $row['max_storage_temp'] = (!empty($row['max_storage_temp_measurement'])
                    && empty($row['max_storage_temp'])) ? "0" : $row['max_storage_temp'];
                $row['price'] = empty($row['price']) ? 12 : $row['price'];
                $row['attribute_set'] = "GS1";
                $row['_attribute_set'] = "GS1";
                $row['category_ids'] = "686";
                $row['price_comp_measurement'] = 'DKK';
                $rows[] = $row;
            } catch (\Exception $e) {
                $this->failedGTINs[] = $e->getMessage();
            }
        };
        return $rows;
    }
}
