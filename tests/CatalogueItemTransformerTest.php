<?php
namespace DataPushCatalogueItemTransformer\tests;

use DataPushCatalogueItemTransformer\CatalogueItemTransformer;

class CatalogueItemTransformerTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
    }
    public function tearDown()
    {
    }
    /**
     * testEmptyTransformerData(): Empty data should return TypeError
     * @return type
     */
    public function testEmptyTransformerData()
    {
        $this->setExpectedException('TypeError');
        CatalogueItemTransformer::transform();
    }
    /**
     * testEmptyTransformerData(): Empty data should return TypeError
     * @return type
     */
    public function testEmptyArrayTransformerData()
    {
        $this->assertEmpty(CatalogueItemTransformer::transform($this->generateAFakeCatalog(0)));
    }
    public function testValidTransformerData()
    {
        $this->assertTrue(count(CatalogueItemTransformer::transform($this->generateAFakeCatalog(10))) === 10);
    }
    public function testInvalidTransformerData()
    {
        $this->setExpectedException('Exception');
        CatalogueItemTransformer::transform($this->generateAFakeCatalog(10, false));
    }
    private function generateAFakeCatalog($qty = 10, $valid = true)
    {
        $catalog = [
            'GS1' => [],
            'SA24' => [],
        ];
        for ($i=0; $i < $qty; $i++) {
            $item = [
                "source" => "6G6",
                "tradeItem" => [
                    "additionalTradeItemIdentification" => [
                        "@value" => "",
                        "@attributes" => [
                            "additionalTradeItemIdentificationTypeCode" => "1245112$i",
                        ],
                    ],
                    "gtin" => "054100768950$i",
                    "ean" => "0001234567$i",
                    "articleNumber" => "348907-$i",
                    "informationProviderOfTradeItem" => [
                        "gln" => "00370000000$i",
                        "vat" => [
                            "cvrNumber" => [
                                "@value" => "035849$i",
                                "@attributes" => [
                                    "languageCode" => "da"
                                ]
                            ]
                        ],
                        "partyName" => "Test party name",
                    ],
                    "isTradeItemAConsumerUnit" => true,
                    "tradeItemInformation" => [
                        "extension" => [
                            "packaging_marking:packagingMarkingModule" => [
                                "packagingMarking" => [
                                    "isPackagingMarkedReturnable" => false,
                                ],
                            ],
                            "trade_item_description:tradeItemDescriptionModule" => [
                                "tradeItemDescriptionInformation" => [
                                    "descriptionShort" => "PANTENE REPAIR 250".$i."ML",
                                    "brandNameInformation" => [
                                        "brandName" => "Pantene"
                                    ]
                                ]
                            ],
                            "trade_item_measurements:tradeItemMeasurementsModule" => [
                                "tradeItemMeasurements" => [
                                    "tradeItemWeight" => [
                                        "netWeight" => [
                                            "@value" => "$i",
                                            "@attributes" => [
                                                "measurementUnitCode" => "GRM"
                                            ]
                                        ],
                                        "grossWeight" => [
                                            "@value" => "$i",
                                            "@attributes" => [
                                                "measurementUnitCode" => "GRM"
                                            ]
                                        ],
                                    ]
                                ]
                            ],
                            "trade_item_stock_information:tradeItemStockInformationModule" => [
                                "warehouse" => [
                                    "name" => "noavision",
                                    "rules" => [
                                        "reorderPoint" => "$i",
                                        "minStock" => "$i",
                                        "leadTime" => "$i",
                                        "notificationDays" => "90"
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ];
            if (!$valid) {
                $itemObjectGS1 = json_decode(json_encode($item), false);
                $itemObjectGS1->tradeItem->gtin = "123".($qty+$i);
                $itemObjectSA24 = json_decode(json_encode($item), false);
            } else {
                $itemObjectGS1 = json_decode(json_encode($item), false);
                $itemObjectSA24 = $itemObjectGS1;
            }
            $catalog['GS1'][] = $itemObjectGS1;
            $catalog['SA24'][] = $itemObjectSA24;
        }
        return $catalog;
    }
}
